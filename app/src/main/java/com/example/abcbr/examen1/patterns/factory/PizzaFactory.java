package com.example.abcbr.examen1.patterns.factory;

/**
 * Created by abcbr on 11/10/2017.
 */

public interface PizzaFactory {

    String PizzaNames[] = {"Cheese Pizza", "Supreme Pizza", "Brazilian Pizza"};

    enum PizzaType {
        CHEESE, SUPREME, BRAZILIAN
    }

    Pizza getPizza(PizzaType pizzaType);

}

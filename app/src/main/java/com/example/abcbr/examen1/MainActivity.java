package com.example.abcbr.examen1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.abcbr.examen1.patterns.factory.ApplicationPizzaFactory;
import com.example.abcbr.examen1.patterns.factory.Pizza;
import com.example.abcbr.examen1.patterns.factory.PizzaFactory;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private Spinner spinner;
    private Button button;
    private PizzaFactory factory;
    private Pizza pizza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        factory = new ApplicationPizzaFactory();

        textView = (TextView) findViewById(R.id.textView);
        spinner = (Spinner) findViewById(R.id.spinner);
        button = (Button) findViewById(R.id.button);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, PizzaFactory.PizzaNames);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);
        spinner.setSelection(0);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selection = spinner.getSelectedItemPosition();
                System.out.println("Valor del spiner: " + selection);
                switch (selection){
                    case 0:
                        pizza = factory.getPizza(PizzaFactory.PizzaType.CHEESE);
                        break;
                    case 1:
                        pizza = factory.getPizza(PizzaFactory.PizzaType.SUPREME);
                        break;
                    case 2:
                        pizza = factory.getPizza(PizzaFactory.PizzaType.BRAZILIAN);
                        break;
                    default:
                        pizza = factory.getPizza(PizzaFactory.PizzaType.CHEESE);
                        break;
                }

                textView.setText(pizza.getIngredients());
            }
        });

    }
}

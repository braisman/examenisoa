package com.example.abcbr.examen1.patterns.factory;

/**
 * Created by abcbr on 11/10/2017.
 */

public interface Pizza {

    String getIngredients();

}

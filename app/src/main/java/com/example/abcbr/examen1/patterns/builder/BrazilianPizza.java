package com.example.abcbr.examen1.patterns.builder;

import com.example.abcbr.examen1.patterns.factory.Pizza;

/**
 * Created by abcbr on 11/10/2017.
 */

public class BrazilianPizza implements Pizza {

    private String pasta = "Pasta ";
    private String pizzaSauce = "Pizza sauce ";
    private String pizzaCheese = "Pizza cheese ";
    private String ingredients = "";

    @Override
    public String getIngredients() {
        return pasta + pizzaSauce + pizzaCheese + ingredients;
    }

    public BrazilianPizza(Builder builder){
        this.ingredients = builder.getIngredients();
    }

    public static class Builder{
        private String ingredients = "";

        public String getIngredients() {
            return ingredients;
        }

        public Builder addMeat(){
            this.ingredients = this.ingredients + "Meat ";
            return this;
        }

        public Builder addCondiment(){
            this.ingredients = this.ingredients + "Condiment ";
            return this;
        }

        public Builder addTomato(){
            this.ingredients = this.ingredients + "Tomato ";
            return this;
        }

        public BrazilianPizza prepare(){
            return new BrazilianPizza(this);
        }

    }
}

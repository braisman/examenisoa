package com.example.abcbr.examen1.patterns.builder;

import com.example.abcbr.examen1.patterns.factory.Pizza;

/**
 * Created by abcbr on 11/10/2017.
 */

public class CheesePizza implements Pizza {

    private String pasta = "Pasta ";
    private String pizzaSauce = "Pizza sauce ";
    private String pizzaCheese = "Pizza cheese ";
    private String ingredients = "";

    @Override
    public String getIngredients() {
        return pasta + pizzaSauce + pizzaCheese + ingredients;
    }

    public CheesePizza(Builder builder){
        this.ingredients = builder.getIngredients();
    }

    public static class Builder{
        private String ingredients = "";

        public String getIngredients() {
            return ingredients;
        }

        public Builder addMozzarella(){
            this.ingredients = this.ingredients + "Mozzarella ";
            return this;
        }

        public Builder addCheddar(){
            this.ingredients = this.ingredients + "Cheddar ";
            return this;
        }

        public Builder addParmesan(){
            this.ingredients = this.ingredients + "Parmesan ";
            return this;
        }

        public CheesePizza prepare(){
            return new CheesePizza(this);
        }

    }
}

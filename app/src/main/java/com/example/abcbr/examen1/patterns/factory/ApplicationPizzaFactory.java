package com.example.abcbr.examen1.patterns.factory;

import com.example.abcbr.examen1.patterns.builder.BrazilianPizza;
import com.example.abcbr.examen1.patterns.builder.CheesePizza;
import com.example.abcbr.examen1.patterns.builder.SupremePizza;

/**
 * Created by abcbr on 11/10/2017.
 */

public class ApplicationPizzaFactory implements PizzaFactory {
    @Override
    public Pizza getPizza(PizzaType pizzaType) {
        Pizza pizza = null;

        switch (pizzaType){
            case CHEESE:
                CheesePizza.Builder builderCheesePizza = new CheesePizza.Builder();
                builderCheesePizza.addCheddar();
                builderCheesePizza.addMozzarella();
                builderCheesePizza.addParmesan();
                pizza = builderCheesePizza.prepare();
                break;
            case SUPREME:
                SupremePizza.Builder builderSupremePizza = new SupremePizza.Builder();
                builderSupremePizza.addMeat();
                builderSupremePizza.addOnion();
                builderSupremePizza.addPepper();
                pizza = builderSupremePizza.prepare();
                break;
            case BRAZILIAN:
                BrazilianPizza.Builder builderBrazilianPizza = new BrazilianPizza.Builder();
                builderBrazilianPizza.addMeat();
                builderBrazilianPizza.addCondiment();
                builderBrazilianPizza.addTomato();
                pizza = builderBrazilianPizza.prepare();
                break;
            default:
                CheesePizza.Builder builder = new CheesePizza.Builder();
                builder.addCheddar();
                builder.addMozzarella();
                builder.addParmesan();
                pizza = builder.prepare();
                break;
        }

        return pizza;
    }
}
